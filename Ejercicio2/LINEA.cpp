//Se inclullen librerias y archivos necesarios
#include <iostream>
using namespace std;
#include "LINEA.hpp"
#include <cctype>
#include <string>

//Inicializa la clase LINEA
LINEA::LINEA(string linea){
    //Se le asignan valores a las variables de la clase privativas
    this-> linea_ingresada = linea;
    this->Nmayusculas = 0;
    this->Nminusculas = 0;
    
    //Se recorre las cadena de string de modo de obtener la cantidad de mayusculas y 
    //minusculas de cada linea entregada
    for (int i=0; i<linea.size()+1; i++){
        if (islower(linea[i])){
            Nminusculas = Nminusculas+1;
        }else if(isupper(linea[i])){
            Nmayusculas = Nmayusculas+1;
        }
    }
}

//Se exportan los datos obtenidos a Main2
int LINEA::mayus(){
    return Nmayusculas;
}

int LINEA::minus(){
    return Nminusculas;
}
