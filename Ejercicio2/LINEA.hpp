#ifndef LINEA_HPP
#define LINEA_HPP

class LINEA{
    private:
        //Se agregan las variables privativas de la clase LINEA
        string linea_ingresada = " ";
        int Nmayusculas;
        int Nminusculas;
    public:
        //Constructor de la clase
        LINEA (string linea);
        //Metodos para exportar la cantidad de mayusculas y minusculas respectivamente
        int mayus();
        int minus();
};

#endif