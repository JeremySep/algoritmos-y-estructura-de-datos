//Se inclullen las librerias y archivos necesarios 
#include <iostream>
using namespace std;
#include "LINEA.hpp"


//Se inicia la aplicacion 
int main(int argc, char **argv){
    //Se asignan variables 
    string linea;
    int N;
    int Nmayus;
    int Nminus;

    //Se piden datos para empezar la ejecutacion del resto del programa
    cout << "Ingrese la cantidad de frases que quiere agregar: "<< endl;

    scanf("%d",&N);

    for (int i=0; i<N; i++){

        cout << "Ingrese la cadena de caracteres: " << endl;

        getline(cin, linea);

        //Se exportan los datos a LINEA (la otra parte del programa)
        LINEA lineas(linea);

        Nmayus = lineas.mayus();

        Nminus = lineas.minus();

        //Se entregan los resultados
        cout << "En la frase N°: " << i << " la cantidad de mayusculas es: " << Nmayus << " y la cantidad de minusculas es: " << Nminus << endl;
    }

    //Finaliza la aplicacion
    return 0;
}