//Se incluyen todas las librerias neceasrias y archivos
#include <iostream>
using namespace std;
#include "Arreglo.hpp"

//Se inicia la clase ingresando un dato tipo entero
Arreglo::Arreglo(int N){
    //Se asignan las variables para su uso en la clase
    this-> numero = N;
    this-> numeros[N];
    this-> resultado_del_array = 0;
    //Se utiliza un ciclo for para realizar los calculos necesarios para obtener un resultado esperado
    for (int i=0; i<N; i++){
        numeros[i] = (i+1)*(i+1);
        resultado_del_array = numeros[i]+ resultado_del_array;
    }
}

//Se exporta el resultado a Main
int Arreglo::get_resultado(){
    return resultado_del_array;
}

