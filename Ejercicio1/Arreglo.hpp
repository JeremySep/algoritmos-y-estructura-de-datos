#ifndef ARREGLO_HPP
#define ARREGLO_HPP
//Se define la clase Arreglo
class Arreglo{
    private:
        //Se definen las variables privativas que se utilizarán después
        int resultado_del_array; 
        int numero; 
        int numeros[];
    public:
        //Constructor de la clase
        Arreglo (int N);
        //Metodo de exportacion del resultado
        int get_resultado();
};

#endif
