//Se incluyen las librerias y archivos necesarios 
#include <iostream>
using namespace std;
#include "Arreglo.hpp"

//Se inicializa la aplicación 
int main(int argc, char **argv){
    //Se generan variables para el trafico de datos entre el usuario y la maquina
    int N;
    int Resultado;
    cout << "Ingrese el tamaño del arreglo: " << endl; 
    cin >> N;
    //Se crea la clase Arreglo y se le entrega el valor numerico N 
    Arreglo Arreglo(N);
    //Se importa el resultado desde Arreglo.cpp
    Resultado = Arreglo.get_resultado();
    cout << "El resultado es: " << Resultado << endl;

    return 0;
}
