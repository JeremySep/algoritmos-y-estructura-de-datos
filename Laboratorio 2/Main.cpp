//Se incluyen librerias y documentos necesarios
#include <iostream>
using namespace std;
#include "Pila.hpp"

//Inicio del programa
int main(int argc, char **argv){

    //Creacion de variables para el funcionamiento del ciclo y valores dados por el usuario
    int exit = 1;
    int maximo;
    int decision;

    //Interaccion con el usuario pidiendo un dato (Tamaño de la lista que se creara)
    cout << "Ingrese el tamño máximo para la pila: " << endl;
    cin >> maximo; 
    Pila Pila(maximo);

    while (exit == 1){//Ciclo para su reutilizacion en la toma de decisiones
        
        //Enumeracion de las acciones posibles para el usuario y el ingreso del dato decision
        cout << "Agregar/push [1]" << endl;
        cout << "Remover/pop [2]" << endl;
        cout << "Ver la pila [3]" << endl;
        cout << "Salir [0]" << endl;
        cin >> decision;
        if (decision == 0){
            exit = 0;
        }
        Pila.toma_decision(decision);
    }

    //Termino del programa
    return 0;
}