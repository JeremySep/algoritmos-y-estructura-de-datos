#ifndef PILA_HPP 
#define PILA_HPP

//Se definen las variables privativas de la clase pila
class Pila{
    private:
        int max;
        int tope;
        bool band;
        int dato;
        int decision;
        int datos[];
        //Se definen los metodo publicos y la llamada a la funcion pila con sus tipos de datos a ingresar ya definidos
    public:
        Pila (int maximo);
        void toma_decision(int decision);
        void pila_vacia(int tope, bool band);
        void pila_llena(int tope, bool band, int max);
        void push(int dato, int tope, int max, int datos[]);
        void pop(int datos[], int tope);
        void print_datos(int datos[], int max);
};
#endif 