//Se incluyen librerias y documentos necesarios
#include <iostream>
using namespace std;
#include "Pila.hpp"

//Se inician la variables a utilizar 
Pila::Pila(int maximo){
    this-> max = maximo;
    this-> datos[max];
    this-> tope = 0;
    this-> band = false;
    for (int i=0; i<max; i++){//Se le asigna un valor 0 considerado como neutro a cada espacio del arreglo 
        datos[i] = 0;
    }
}

//Metodo para saber si la pila esta llena. Devuelve un valor verdadero en caso de que este llena
void Pila::pila_llena(int tope, bool band, int max){
    if (this->tope == max){
        this->band = true;//pila llena
    }
    else{
        this->band = false;//pila con espacio
    }
}

//Metodo para saber si la pila esta vacia. Devuelve un valor verdadero en caso de que este vacia
void Pila::pila_vacia(int tope, bool band){
    if (this->tope == -1){
        this->band = true;//pila vacia
        this->tope = this->tope+1;
    }
    else {
        this->band = false;//pila con datos
    }
}

//Metodo para agregar datos a la pila con un dato dado por el usuario en orden creciente y en caso de estar lleno se envia un mensaje
void Pila::push(int dato, int tope, int max, int datos[]){
    pila_llena(tope, band, max);
    if (band == true){//Mensaje de advertencia al usuario
        cout << "Desborde de la pila, imposible realizar acción" << endl;
    }
    else{//Se agregan los datos
        cout << "Agregue el dato a guardar: " << endl;
        cin >> dato;
        if (this->tope<0){
            this->tope = this->tope+1;
        }
        datos[this->tope] = dato; 
        this->tope = this->tope+1;
    }
}

//Metodo para eliminar el valor tope de la lista, en caso de que este vacia envia un mensaje
void Pila::pop(int datos[], int tope){
    pila_vacia(tope, band);
    if (band == true){//Envia mensaje de advertencia 
        cout << "Subdesborde de la pila, imposible realizar acción" << endl; 
    }
    else{//Elimina un valor de la lista reemplazandolo por cero como numero neutro
        this->tope = this->tope-1;
        datos[this->tope] = 0;
    }
}

//Metodo imprime los valores de la lista uno por linea separando la lista por lineas
void Pila::print_datos(int datos[], int max){
    cout << "********************" << endl;
    for (int i=0; i<max; i++){//Ciclo para la impresion de los datos
        cout << datos[i] << endl;
    }
    cout << "********************" << endl;
}

//Se importa la variable decision desde main para usar cualquiera de las acciones alli descritas
void Pila::toma_decision(int decision){
    this-> decision = decision;
    if (decision == 1){//Agregar dato
        push(dato, tope, max, datos);
    }
    else if (decision == 2){//Eliminar dato
        pop(datos, tope);
    }
    else if (decision == 3){//Imprimir todos los datos
        print_datos(datos, max);
    }   
}
